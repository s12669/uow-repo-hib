package com.example.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheStorage<TEntity> {
    private Long timeToLive;
    private Map<String, CacheStorageObject> items;
    private final Object token = new Object();

    public CacheStorage(Long timeToLive) {
        this.items = new HashMap<>();
        this.timeToLive = timeToLive;
    }

    protected class CacheStorageObject {
        private Long timeLastUsed;
        private TEntity value;

        public CacheStorageObject(TEntity value) {
            this.timeLastUsed = System.currentTimeMillis();
            this.value        = value;
        }

        public Long getTimeLastUsed() {
            return timeLastUsed;
        }

        public void setTimeLastUsed(Long timeLastUsed) {
            this.timeLastUsed = timeLastUsed;
        }

        public TEntity getValue() {
            return value;
        }

        public void setValue(TEntity value) {
            this.value = value;
        }
    }

    public TEntity get(String key) {
        if (!this.items.containsKey(key)) {
            return null;
        }

        synchronized (this.token) {
            CacheStorageObject tempCacheObject = this.items.get(key);
            tempCacheObject.setTimeLastUsed(System.currentTimeMillis());
            return tempCacheObject.getValue();
        }
    }

    public void put(String key, TEntity value) {
        CacheStorageObject tempCacheObject = this.items.getOrDefault(
                key, new CacheStorageObject(value)
        );

        synchronized (this.token) {
            tempCacheObject.setTimeLastUsed(System.currentTimeMillis());
            this.items.put(key, tempCacheObject);
        }
    }

    public void remove(String key) {
        if (!this.items.containsKey(key)) {
            return;
        }

        synchronized (this.token) {
            this.items.remove(key);
        }
    }

    public void cleanup() {
        List<String> keysToDelete = new ArrayList<>();

        synchronized (this.token) {
            Long currentTime  = System.currentTimeMillis();

            for (Map.Entry<String, CacheStorageObject> entry : this.items.entrySet()) {
                String             key   = entry.getKey();
                CacheStorageObject value = entry.getValue();

                if (currentTime - value.getTimeLastUsed() > this.timeToLive) {
                    keysToDelete.add(key);
                }
            }
        }

        for (String key : keysToDelete) {
            synchronized (this.token) {
                this.items.remove(key);
            }
        }
    }

    public Integer size() {
        synchronized (this.token) {
            return this.items.size();
        }
    }
}