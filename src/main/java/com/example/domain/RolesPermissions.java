package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_SYS_PERMISSIONS")
public class RolesPermissions extends Entity{
    private int roleId;
    @Column(name = "permission")
    private int permissionId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }
}
