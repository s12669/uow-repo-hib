package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_SYS_ENUMS")
public class EnumerationValue extends Entity {
    @Column(name = "int_key")
    private String intKey;
    @Column(name = "string_key")
    private String stringKey;
    @Column(name = "value")
    private String value;
    @Column(name = "name")
    private String enumerationName;

    public EnumerationValue(String enumerationName, String value) {
        this.enumerationName = enumerationName;
        this.value = value;
    }

    public String getIntKey() {
        return intKey;
    }

    public void setIntKey(String intKey) {
        this.intKey = intKey;
    }

    public String getEnumerationName() {
        return enumerationName;
    }

    public void setEnumerationName(String enumerationName) {
        this.enumerationName = enumerationName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStringKey() {
        return stringKey;
    }

    public void setStringKey(String stringKey) {
        this.stringKey = stringKey;
    }
}
