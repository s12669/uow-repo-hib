package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_SYS_ROLES")
public class UserRoles extends Entity{
    @Column(name = "user_id")
    private int userId;
    @Column(name = "role_id")
    private int roleId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
