package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_P_PHONE")
public class PhoneNumber extends Entity{
    @Column(name = "country_prefix")
    private int countryPrefix;
    @Column(name = "city_prefix")
    private int cityPrefix;
    @Column(name = "number")
    private long number;
    @Column(name = "type_id")
    private int typeId;

    public int getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(int countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public int getCityPrefix() {
        return cityPrefix;
    }

    public void setCityPrefix(int cityPrefix) {
        this.cityPrefix = cityPrefix;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}
