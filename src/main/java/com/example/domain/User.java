package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
@Table(name = "T_SYS_USERS")
public class User extends Entity {
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    private List<UserRoles> userRoles = new ArrayList<UserRoles>();
    private List<RolesPermissions> rolesPermissions = new ArrayList<RolesPermissions>();
    private List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
    private List<Address> addresses = new ArrayList<Address>();


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRoles> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoles> userRoles) {
        this.userRoles = userRoles;
    }

    public List<RolesPermissions> getRolesPermissions() {
        return rolesPermissions;
    }

    public void setRolesPermissions(List<RolesPermissions> rolesPermissions) {
        this.rolesPermissions = rolesPermissions;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
