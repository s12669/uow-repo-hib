package com.example.builder.implementation;

import com.example.builder.IEntityBuilder;
import com.example.domain.EnumerationValue;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EnumerationValueBuilder implements IEntityBuilder<EnumerationValue> {
    public EnumerationValue build(ResultSet rs) throws SQLException {
        EnumerationValue enumerationValue = new EnumerationValue();

        enumerationValue.setIntKey(rs.getString("key"));
        enumerationValue.setEnumerationName(rs.getString("name"));
        enumerationValue.setValue(rs.getString("value"));

        return enumerationValue;
    }
}
