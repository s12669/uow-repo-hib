package com.example.repository;

import com.example.repository.implementation.PagingInfo;

public interface IRepository<TEntity> {

    void add(TEntity entity);

    void delete(TEntity entity);

    void modify(TEntity entity);

}
