package com.example.repository;

import com.example.domain.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {
    EnumerationValue withName(String name);

    EnumerationValue withStringKey(String key);
}
