package com.example.repository.implementation;

import com.example.domain.User;
import com.example.builder.IEntityBuilder;
import com.example.checker.rules.LoginRule;
import com.example.checker.rules.PasswordRule;
import com.example.repository.IUserRepository;
import com.example.uowrepository.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository extends Repository<User> implements IUserRepository {

    protected PreparedStatement selectByLogin;
    protected PreparedStatement selectByLoginAndPassword;

    protected String sqlTemplateSelectByLogin =
            "SELECT * FROM " + this.getTableName() + " WHERE login=?";
    protected String sqlTemplateSelectByLoginAndPassword =
            this.sqlTemplateSelectByLogin + " AND password=?";

    public UserRepository(Connection connection, IEntityBuilder<User> entityBuilder, UnitOfWork unitOfWork) {
        super(connection, entityBuilder, unitOfWork);

        try {
            this.selectByLogin = connection.prepareStatement(this.sqlTemplateSelectByLogin);
            this.selectByLoginAndPassword = connection.prepareStatement(this.sqlTemplateSelectByLoginAndPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.ruleChecker.addRule(new LoginRule());
        this.ruleChecker.addRule(new PasswordRule());
    }

    @Override
    public User withLogin(String login) {
        try {
            this.selectByLogin.setString(1, login);
            ResultSet resultSet = this.selectByLogin.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User withLoginAndPassword(String login, String password) {
        try {
            this.selectByLoginAndPassword.setString(1, login);
            this.selectByLoginAndPassword.setString(2, password);
            ResultSet resultSet = this.selectByLoginAndPassword.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User setupPermissions(User user) {
        return null;
    }

    @Override
    public User setupRoles(User user) {
        return null;
    }

    @Override
    protected void prepareQueryForUpdate(User entity) throws SQLException {
        this.update.setString(1, entity.getLogin());
        this.update.setString(2, entity.getPassword());
        this.update.setString(3, entity.getId());
    }

    @Override
    protected void prepareQueryForInsert(User entity) throws SQLException {
        this.insert.setString(1, entity.getLogin());
        this.insert.setString(2, entity.getPassword());
    }

    @Override
    protected String getTableName() {
        return "t_sys_users";
    }

    @Override
    protected String getSqlTemplateForInsert() {
        return "INSERT INTO " + this.getTableName() + "(login, password) VALUES(?, ?)";
    }

    @Override
    protected String getSqlTemplateForUpdate() {
        return "UPDATE " + this.getTableName() + " SET (login, password)=(?, ?) WHERE id=?";
    }
}
