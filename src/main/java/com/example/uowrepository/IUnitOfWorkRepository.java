package com.example.uowrepository;

public interface IUnitOfWorkRepository<TEntity> {
    void persistAdd(TEntity entity);

    void persistDelete(TEntity entity);

    void persistUpdate(TEntity entity);
}
