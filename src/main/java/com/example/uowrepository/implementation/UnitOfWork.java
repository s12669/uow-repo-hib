package com.example.uowrepository.implementation;


import com.example.domain.Entity;
import com.example.domain.EntityState;
import com.example.uowrepository.IUnitOfWork;
import com.example.uowrepository.IUnitOfWorkRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class UnitOfWork implements IUnitOfWork {

    private Map<Entity, IUnitOfWorkRepository> entities = new LinkedHashMap<Entity, IUnitOfWorkRepository>();
    private Connection connection;

    @Override
    public void saveChanges() {

        for(Entity entity: entities.keySet())
        {
            switch(entity.getState())
            {
                case Modified:
                    entities.get(entity).persistUpdate(entity);
                    break;
                case Deleted:
                    entities.get(entity).persistDelete(entity);
                    break;
                case New:
                    entities.get(entity).persistAdd(entity);
                    break;
                case UnChanged:
                    break;
                default:
                    break;}
        }

        try {
            connection.commit();
            entities.clear();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void undo() {
        entities.clear();
    }

    @Override
    public void markAsNew(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.New);
        entities.put(entity, repo);
    }

    @Override
    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.Deleted);
        entities.put(entity, repo);
    }

    @Override
    public void markAsChanged(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.Modified);
        entities.put(entity, repo);
    }
}
