package com.example.checker.rules;

import com.example.domain.Person;
import com.example.checker.CheckResult;
import com.example.checker.ICanCheckRule;
import com.example.checker.RuleResult;

public class EmailRule implements ICanCheckRule<Person> {

    public CheckResult checkRule(Person entity) {

        int at = 0;

        if (entity.getEmail() != null) {
            if (!entity.getEmail().equals("")) {
                for (int i = 0; i < entity.getEmail().length(); i++) {
                    if (entity.getEmail().substring(i, i + 1).equals("@")) {
                        at +=1;
                    }
                }
            }
        }
        if (entity.getEmail() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getEmail().equals(""))
            return new CheckResult("", RuleResult.Error);
        if (at != 1)
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}
