package com.example.checker;

public interface ICanCheckRule<TEntity> {

    public CheckResult checkRule(TEntity entity);
}