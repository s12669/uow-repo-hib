package com.example;

import com.example.caching.CacheStorage;
import com.example.domain.EnumerationValue;
import com.example.domain.User;
import com.example.repository.implementation.RepositoryCatalog;
import com.example.uowrepository.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class App {
    public static void main(String[] args) {
        EnumerationValue sysEnum1 = new EnumerationValue("xyz", "789");
        EnumerationValue sysEnum2 = new EnumerationValue("abc", "123");

        CacheStorage<EnumerationValue> cacheStorage = new CacheStorage<>(5000L);

        cacheStorage.put(sysEnum1.getEnumerationName(), sysEnum1);
        cacheStorage.put(sysEnum2.getEnumerationName(), sysEnum2);

        System.out.println(cacheStorage.size());

        cacheStorage.cleanup();
        System.out.println(cacheStorage.size());

        try {
            Thread.sleep(6000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        cacheStorage.cleanup();
        System.out.println(cacheStorage.size());

        String databaseUrl = "jdbc:postgresql://localhost:5432/hibernatedb";

        User def = new User();
        def.setLogin("user");
        def.setPassword("pass");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            UnitOfWork unitOfWork = new UnitOfWork(connection);
            RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);

            repositoryCatalog.users().save(def);
            unitOfWork.saveChanges();

            List<User> usersFromDatabase = repositoryCatalog.users().getAll();
            for (User user : usersFromDatabase) {
                System.out.println(user.getId() + " " + user.getLogin() + " " + user.getPassword());
            }

            System.out.println(repositoryCatalog.users().count());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}