package com.example.checker.rules;
import com.example.domain.User;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PasswordRuleTest {
    PasswordRule rule = new PasswordRule();
    @Test
    public void checker_should_check_if_the_user_password_is_not_null() {
        User u = new User();
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_user_password_is_not_empty() {
        User u = new User();
        u.setPassword("");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_user_password_is_long_enough() {
        User u = new User();
        u.setPassword("1234567");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_return_OK_if_the_user_password_is_not_null() {
        User u = new User();
        u.setPassword("kowal123");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }
}
