package com.example.checker.rules;

import com.example.domain.Person;
import com.example.checker.CheckResult;
import com.example.checker.RuleResult;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NipRuleTest {
    NipRule rule = new NipRule();

    @Test
    public void checker_should_check_if_the_nip_is_not_null() {
        Person p = new Person();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_nip_is_not_empty() {
        Person p = new Person();
        p.setNip("");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_nip_is_10_characters_long() {
        Person p = new Person();
        p.setNip("54215498215984");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));
    }

    @Test
    public void checker_should_check_if_check_digit_is_not_10() {
        Person p = new Person();
        p.setNip("1234567890");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));
    }

    @Test
    public void checker_should_return_OK_if_nip_check_digit_is_correct() {
        Person p = new Person();
        p.setNip("6614374868");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }

}
