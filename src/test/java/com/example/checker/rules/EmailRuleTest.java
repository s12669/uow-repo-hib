package com.example.checker.rules;

import com.example.checker.CheckResult;
import com.example.checker.RuleResult;
import com.example.domain.Person;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EmailRuleTest {
    EmailRule rule = new EmailRule();

    @Test
    public void checker_should_check_if_the_person_email_is_not_null() {
        Person p = new Person();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_person_email_is_not_empty() {
        Person p = new Person();
        p.setEmail("");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_return_OK_if_the_email_is_in_correct_format() {
        Person p = new Person();
        p.setEmail("jankow@wp.pl");
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }
}
